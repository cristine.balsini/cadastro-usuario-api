import React from "react";
import { useParams, useHistory } from "react-router-dom";
import postFeedback from "./feedback-post.js";
import "./feedback-form.css";
import { Form, Input, InputNumber, Button } from "antd";
import { SendOutlined } from "@ant-design/icons";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

const FeedbackForm = () => {
  const [form] = Form.useForm();
  const params = useParams();
  const history = useHistory();

  const onFinish = (values) => {
    postFeedback(values, params.id, history);
  };

  return (
    <div className="Feedback-form">
      <h1 className="feed-Title">New Feedback</h1>
      <Form {...layout} form={form} name="feedbacks" onFinish={onFinish}>
        <Form.Item
          name="name"
          label="User"
          rules={[
            {
              required: true,
              message: "Digite nome do usuário",
              whitespace: false,
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="comment"
          label="Comment"
          rules={[
            {
              required: true,
              message: "Digite seu feedback",
              whitespace: false,
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          name="grade"
          label="Grade"
          rules={[
            {
              required: true,
              whitespace: false,
              message: "Digite a nota de 0 a 10",
              type: "number",
              min: 0,
              max: 10,
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
          <Button type="primary" htmlType="submit" icon={<SendOutlined />}>
            Enviar
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
export default FeedbackForm;
