const postFeedback = (values, id, history) => {
  values = {
    feedback: { ...values },
  };

  fetch(`https://ka-users-api.herokuapp.com//users/${id}/feedbacks/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: localStorage.getItem("token"),
    },
    body: JSON.stringify(values),
  }).then(() => {
    history.push(`/users/${id}/feedback`);
  });
};

export default postFeedback;
