import React from "react";
import "./App.css";
import Router from "./routes";
import "antd/dist/antd.css";

const App = () => {
  return (
    <div className="App">
      <Router />
    </div>
  );
};

export default App;
