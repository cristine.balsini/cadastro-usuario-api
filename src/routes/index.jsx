import React, { useState } from "react";
import { Switch, Route, Link, useLocation } from "react-router-dom";
import Register from "../pages/register/";
import Login from "../pages/login";
import Users from "../pages/users";
import Feedback from "../pages/feedback";
import FeedbackForm from "../components/feedback-form";

import "./route.css";
import { Layout, Menu } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  KeyOutlined,
  UploadOutlined,
  UsergroupAddOutlined,
} from "@ant-design/icons";

const { Header, Sider, Content } = Layout;

const Router = () => {
  const [showUserNav, setUserNav] = useState(false);
  const [collapsed, setCollapsed] = useState(false);
  const location = useLocation();

  const getKey = () => {
    if (location.pathname === "/") {
      return "1";
    } else if (location.pathname === "/register") {
      return "2";
    } else if (location.pathname === "/users") {
      return "3";
    }
  };
  const toggle = () => {
    setCollapsed(!collapsed);
  };

  return (
    <div>
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" selectedKeys={getKey()}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              <Link to="/">Login</Link>
            </Menu.Item>
            <Menu.Item key="2" icon={<KeyOutlined />}>
              <Link to="/register">Registrar</Link>
            </Menu.Item>
            {showUserNav && (
              <Menu.Item defaultSelectedKeys="1" icon={<UploadOutlined />}>
                <Link
                  onClick={() => {
                    localStorage.clear();
                    setUserNav(false);
                  }}
                  to="/"
                >
                  Logout
                </Link>
              </Menu.Item>
            )}
            {showUserNav && (
              <Menu.Item key="3" icon={<UsergroupAddOutlined />}>
                <Link to="/users">Usuários</Link>
              </Menu.Item>
            )}
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: toggle,
              }
            )}
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
            }}
          >
            <Switch>
              <Route exact path="/register">
                <Register />
              </Route>
              <Route exact path="/">
                <Login />
              </Route>

              <Route exact path="/users">
                <Users
                  className="user-layout"
                  setUserNav={setUserNav}
                  showUserNav={showUserNav}
                />
              </Route>
              <Route exact path="/users/:id/feedback">
                <Feedback />
              </Route>
              <Route exact path="/users/:id/feedback/new">
                <FeedbackForm />
              </Route>
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default Router;
