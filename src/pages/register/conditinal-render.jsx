import React from "react";
import { Spin, Alert } from "antd";
import { Link } from "react-router-dom";

export const verifyState = (failRegister) => {
  if (failRegister === true) {
    return (
      <Spin tip="Você será redirecionado">
        <Alert message="Cadastrado com sucesso!" description="" type="info" />
      </Spin>
    );
  }
};
