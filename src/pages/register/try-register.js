const url = "https://ka-users-api.herokuapp.com/users";

export const tryRegister = (values, history, setFailRegister) => {
  values = { user: { ...values } };

  fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(values),
  }).then((res) => {
    if (res.status === 422) {
      setFailRegister(false);
      setTimeout(() => {
        // history.push("/");
      }, 5000);
    } else if (res.status === 201) {
      setFailRegister(true);
      setTimeout(() => {
        history.push("/");
      }, 5000);
    }
  });
};
