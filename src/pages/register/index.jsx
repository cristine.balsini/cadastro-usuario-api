import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import "./register.css";
import { Form, Input, Tooltip, Button, Spin, Alert, Result } from "antd";
import { QuestionCircleOutlined, KeyOutlined } from "@ant-design/icons";
import { tryRegister } from "./try-register";
import { verifyState } from "./conditinal-render";

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const RegistrationForm = () => {
  const [form] = Form.useForm();
  const [failRegister, setFailRegister] = useState("");
  const history = useHistory();

  const onFinishRegister = (values) => {
    tryRegister(values, history, setFailRegister);
  };

  return (
    <div>
      {failRegister === false ? (
        <Result
          status="500"
          title="422"
          subTitle="Ooops, parece que você já tem cadastro! Acesse a home para fazer login."
          extra={
            <Button type="primary">
              <Link to="/">Ir para home</Link>
            </Button>
          }
        />
      ) : (
        <div className="Registration-form">
          <Form
            {...formItemLayout}
            form={form}
            label="Register"
            name="register"
            onFinish={onFinishRegister}
          >
            <h1>Cadastro de usuário</h1>
            <Form.Item
              name="name"
              label={
                <span>
                  Nome&nbsp;
                  <Tooltip title="Qual é o seu nome de registro?">
                    <QuestionCircleOutlined />
                  </Tooltip>
                </span>
              }
              rules={[
                {
                  required: true,
                  message: "Você precisa dizer o seu nome!",
                  whitespace: false,
                },
              ]}
            >
              <Input placeholder="Nome completo" />
            </Form.Item>
            <Form.Item
              name="user"
              label={
                <span>
                  Usuário&nbsp;
                  <Tooltip title="Este será o seu nome no login. Anota direitinho ai!">
                    <QuestionCircleOutlined />
                  </Tooltip>
                </span>
              }
              rules={[
                {
                  required: true,
                  message: "Você precisa de um nome de usuário!",
                  whitespace: true,
                },
              ]}
            >
              <Input placeholder="Usuário" />
            </Form.Item>
            <Form.Item
              name="email"
              label="E-mail"
              rules={[
                {
                  type: "email",
                  message: "Formato de e-mail inválido!",
                },
                {
                  required: true,
                  message: "Por favor, digite o seu e-mail!",
                },
              ]}
            >
              <Input placeholder="E-mail" />
            </Form.Item>
            <Form.Item
              name="password"
              label="Senha"
              rules={[
                {
                  required: true,
                  message: "Senha é necessária!",
                },
              ]}
              hasFeedback
            >
              <Input.Password placeholder="Senha" />
            </Form.Item>
            <Form.Item
              name="password_confirmation"
              label="Confirmar senha"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Necessário confirmar senha!",
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject("A senha digitada não confere!");
                  },
                }),
              ]}
            >
              <Input.Password placeholder="Confirmar senha" />
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit" icon={<KeyOutlined />}>
                Registrar
              </Button>
            </Form.Item>
            <div>{verifyState(failRegister)}</div>
          </Form>
        </div>
      )}
    </div>
  );
};

export default RegistrationForm;
