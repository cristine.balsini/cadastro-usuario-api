export const tryLogin = ({
  error,
  setError,
  values,
  token,
  setToken,
  history,
}) => {
  fetch("https://ka-users-api.herokuapp.com/authenticate", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
    body: JSON.stringify(values),
  })
    .then((resp) => resp.json())
    .then((resp) => {
      if (resp.error) {
        setError((resp.error = "Credenciais inválidas."));
      }
      if (!resp.error) {
        setToken(resp.auth_token);
        localStorage.setItem("token", resp.auth_token);
        history.push("/users");
      }
    });
};
