import React, { useState } from "react";
import { Form, Input, Button, Alert } from "antd";
import { UserOutlined, LockOutlined, LoginOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import { tryLogin } from "./try-login.js";

import "./login.css";

const Login = (props) => {
  const [token, setToken] = useState(localStorage.getItem("token"));
  const [error, setError] = useState("");

  const history = useHistory();

  const onFinish = (values) => {
    tryLogin({ error, setError, values, token, setToken, history });
  };



  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="Login-form">
      <h1>Login</h1>
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Usuário"
          name="user"
          rules={[
            {
              required: true,
              message: "Por favor, insira seu usuário!",
            },
          ]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Usuário"
          />
        </Form.Item>
        <Form.Item
          label={<span>Senha&nbsp;&nbsp;&nbsp;</span>}
          name="password"
          rules={[
            {
              required: true,
              message: "Por favor, insira sua senha!",
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Senha"
          />
        </Form.Item>
        <Form.Item></Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
            icon={<LoginOutlined />}
          >
            Entrar
          </Button>
        </Form.Item>
        <Form.Item className="Error-message">
          {error && (
            <Alert
              message={error}
              description="Verifique seu usuário ou senha."
              type="error"
              showIcon
            />
          )}
        </Form.Item>
      </Form>
    </div>
  );
};
export default Login;
