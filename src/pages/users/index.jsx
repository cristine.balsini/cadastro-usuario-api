import React, { useState, useEffect } from "react";
import "./style.css";
import "antd/dist/antd.css";

import UserList from "./user-list";

import { Layout } from "antd";

const listUser = (props) => {
  fetch("https://ka-users-api.herokuapp.com/users", {
    headers: {
      Authorization: localStorage.getItem("token"),
    },
  })
    .then((resp) => resp.json())
    .then((resp) => {
      props.setUsers(resp);
    });
};

const Users = (props) => {
  const [users, setUsers] = useState([]);
  const [name, setName] = useState([]);

  useEffect(() => listUser({ users, setUsers }), []);

  const getName = ({ target: { value } }) => {
    setName(value);
  };
  const findUser = users.find((item) => item.user === name);

  return (
    <Layout>
      {props.setUserNav(true)}

      <input
        className="input-layout"
        placeholder="busca por usuário"
        value={name}
        onChange={getName}
      />

      {findUser && <UserList list={[findUser]} />}
      {!findUser && <UserList list={users} />}
    </Layout>
  );
};
export default Users;
