import React from "react";
import { Link } from "react-router-dom";
import "./style.css";
import "antd/dist/antd.css";

import { Table, Space } from "antd";
const { Column, ColumnGroup } = Table;

const UserList = (props) => {
  return (
    <Table className="List" dataSource={props.list}>
      <ColumnGroup>
        <Column title="ID" dataIndex="id" key="id" />
        <Column title="Name" dataIndex="name" key="name" />
        <Column title="User" dataIndex="user" key="user" />
        <Column title="Email" dataIndex="email" key="email" />
        <Column
          title="Feedback"
          key="action"
          render={(list) => (
            <Space size="middle">
              <Link to={`/users/${list.id}/feedback`}>Feedbacks</Link>
            </Space>
          )}
        />
      </ColumnGroup>
    </Table>
  );
};
export default UserList;
