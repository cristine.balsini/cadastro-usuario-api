export const userFeedbackList = (props, id) => {
  fetch(`https://ka-users-api.herokuapp.com/users/${id}/feedbacks`, {
    headers: {
      Authorization: localStorage.getItem("token"),
    },
  })
    .then((res) => res.json())
    .then((res) => {
      props.setUserFeedbacks(res);
    });
};
