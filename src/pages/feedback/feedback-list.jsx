import React from "react";
import { Link } from "react-router-dom";
import "antd/dist/antd.css";
import { Card, Col, Row } from "antd";

const FeedbackList = (props) => {
  return (
    <div>
      {props.list.map((item, index) => (
        <Row key={index} gutter={10}>
          <Col span={2}>
            <Card bordered={true}>{item.id}</Card>
          </Col>
          <Col span={5}>
            <Card bordered={true}>{item.name}</Card>
          </Col>
          <Col span={5}>
            <Card bordered={true}>Comentário</Card>
          </Col>
          <Col span={7}>
            <Card bordered={true}>Nota</Card>
          </Col>

          <Card bordered={true}>
            <Link to={`/users/feedbacks/${item.id}`}>New Feedback</Link>
          </Card>
        </Row>
      ))}
    </div>
  );
};

export default FeedbackList;
