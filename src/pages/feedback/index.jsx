import React, { useState, useEffect } from "react";
import { useParams, Link, useHistory } from "react-router-dom";
import { userFeedbackList } from "./user-feedback";
import { Table, Button, Radio } from "antd";
import { SmileTwoTone } from "@ant-design/icons";
const { Column, ColumnGroup } = Table;

const Feedback = () => {
  const [userFeedbacks, setUserFeedbacks] = useState([]);
  const [size, setSize] = useState();
  const history = useHistory();
  const params = useParams();

  useEffect(() => userFeedbackList({ setUserFeedbacks }, params.id), []);

  const handleSizeChange = (e) => {
    setSize({ size: e.target.value });
  };

  const handleNewFeedback = (e) => {
    history.push(`/users/${params.id}/feedback/new`);
  };

  return (
    <div>
      <Table className="List" dataSource={userFeedbacks}>
        <ColumnGroup>
          <Column title="ID" dataIndex="id" key="id" />
          <Column title="Name" dataIndex="name" key="name" />
          <Column title="Comment" dataIndex="comment" key="comment" />
          <Column title="Grade" dataIndex="grade" key="grade" />
        </ColumnGroup>
      </Table>
      <Button
        style={{ marginTop: 50 + "px" }}
        onClick={handleNewFeedback}
        type="primary"
        icon={<SmileTwoTone />}
        size={size}
      >
        Novo feedback
      </Button>
    </div>
  );
};

export default Feedback;
